# Set up basic Kerberos configuration and allow logins via Kerberos rlogin and
# company.
#
# **********************************************************************
# NOTE: If you wish to override the file /etc/krb.conf in your own class,
# and you are using the "source" parameter, be sure to undefine the
# "content" parameter or you will get an error. Example:
#
#   class s_myclass {
#     include kerberos
#
#     File['/etc/krb5.conf'] {
#       source  => 'puppet:///modules/s_accounts/etc/krb5.conf',
#       content => undef,
#     }
#   }
# **********************************************************************
#
#
# $krb_env: Which kerberos environment to use. Must be one of:
#   'prod', 'uat', or 'test'.
#   Default: 'prod'
#
# $prefer_tcp: Normal kerberos traffic uses UDP, but some applications
#   (lookin' at you Java!) work better with TCP. Set this parameter to
#   "true" to force the client to prefer TCP to UDP.
#   Default: false

class kerberos(
  $prefer_tcp         = false,
  $krb_env            = 'prod',
  $krb_implementation = 'mit',
){

  # We only allow the 'prod', 'uat', and 'test' environments.
  case $krb_env {
    'prod', 'uat', 'test': {}
    default: { fail("unrecognized kerberos environment '${krb_env}'") }
  }

  case $krb_implementation {
    'heimdal', 'mit': {}
    default: {
      fail("only recognize 'heimdal' and 'mit' implemetations (case sensitive)")
    }
  }

  case $::osfamily {
    'RedHat': {
      package { 'krb5-workstation': ensure => present }
    }
    'Debian': {
      case $krb_implementation {
        'heimdal': {
          package {'heimdal-clients': ensure => present; }
        }
        'mit': {
          ensure_packages(['kstart','krb5-user'], {'ensure' => 'present'})
        }
      }
    }
    default: {
      fail("unsupported OS ${::operatingsystem}")
    }
  }

  # Basic Kerberos configuration.
  file { '/etc/krb5.conf':
    content => template('kerberos/etc/krb5.conf.erb')
  }
}

